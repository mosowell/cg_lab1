﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;


namespace Graphics_lab_1
{
    public partial class Form1 : Form
    {
        List<Point> points = new List<Point>();
        public Form1()
        {
            InitializeComponent();

        }
        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            Graphics g = Graphics.FromHwnd(this.Handle);
            if (e.Button == MouseButtons.Left)
            {
                points.Add(new Point(e.X, e.Y));
                Pen blackBrush = new Pen(new SolidBrush(Color.Black), 2);
                g.DrawRectangle(blackBrush, e.X, e.Y, 1, 1);
            }
            if (e.Button == MouseButtons.Right)
            {
                g.Clear(Color.White);
                for (int i = 0; i < points.Count - 1; i++) 
                {
                    drawLine(points[i].X, points[i].Y, points[i + 1].X, points[i + 1].Y);
                }

                // Cоединяем первую и последнюю
                drawLine(points[0].X, points[0].Y, points[points.Count - 1].X, points[points.Count - 1].Y); 
            }
        }
       void drawLine(int x1, int y1, int x2, int y2)
       {
           Graphics g = Graphics.FromHwnd(this.Handle);
           Pen redBrush = new Pen(new SolidBrush(Color.Red), 2);
           // Длина отрезка по X
           int deltaX = Math.Abs(x2 - x1);
           // Длина отрезка по Y
           int deltaY = Math.Abs(y2 - y1);
           int signX = x1 < x2 ? 1 : -1;
           int signY = y1 < y2 ? 1 : -1;
           int error = deltaX - deltaY;
          
           g.DrawRectangle(redBrush, x2, y2, 1, 1);

           while (x1 != x2 || y1 != y2)
           {
               g.DrawRectangle(redBrush, x1, y1, 1, 1);
               int error2 = error*2;
               
               if (error2 > -deltaY)
               {
                   error -= deltaY;
                   x1 += signX;
               }
               if (error2 < deltaX)
               {
                   error += deltaX;
                   y1 += signY;
               }
           }
       }
    }
}

